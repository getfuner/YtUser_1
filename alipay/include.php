<?php

require_once 'function.php';
require_once 'api.php';

DefinePluginFilter('Filter_Plugin_AlipayLogin_Succeed');
DefinePluginFilter('Filter_Plugin_AlipayLogin_Failed');
DefinePluginFilter('Filter_Plugin_AlipayPayReturn_Succeed');
DefinePluginFilter('Filter_Plugin_AlipayPayReturn_Failed');
DefinePluginFilter('Filter_Plugin_AlipayPayNotice_Succeed');
DefinePluginFilter('Filter_Plugin_AlipayBatchTrans_Notice');
#注册插件
RegisterPlugin('alipay', 'ActivePlugin_alipay');

function ActivePlugin_alipay()
{
    Add_Filter_Plugin('Filter_Plugin_AlipayPayNotice_Succeed','Alipay_Filter_Plugin_AlipayPayNotice_Succeed');
    Add_Filter_Plugin('Filter_Plugin_AlipayPayReturn_Succeed','Alipay_Filter_Plugin_AlipayPayReturn_Succeed');    
}

function Alipay_Filter_Plugin_AlipayPayReturn_Succeed(&$data){       	   	    	 	 	 		      	    	    		     	
    global $zbp;    
    
    $LogID=$data['out_trade_no'];    			   	 
    if(empty($LogID)) die();
    $YtUser_buy_Table='zbp_buy';
    $YtUser_buy_DataInfo=array(
        'ID' => array('buy_ID', 'integer', '', 0), 
        'OrderID' => array('buy_OrderID', 'string', 30, 0),   
        'LogID' => array('buy_LogID', 'integer', '', 0), 
        'AuthorID' => array('buy_AuthorID', 'integer', '', 0), 
        'Title' => array('buy_Title', 'string', 255, ''),
        'State' => array('buy_State', 'integer', '', 0), 
        'PostTime' => array('buy_PostTime', 'integer', '', 0),
        'Pay' => array('buy_Pay', 'integer', '', 0), 
        'Express' => array('buy_Express', 'string', '', ''), 
        'IP' => array('buy_IP', 'string', 15, ''),
    ); 

    $sql = $zbp->db->sql->Select($YtUser_buy_Table, array('*'), array(array('=','buy_OrderID',$LogID)), null, null, null);
    $array = $zbp->db->Query($sql);
    
    Redirect($array['0']['buy_Title']);

}

function Alipay_Filter_Plugin_AlipayPayNotice_Succeed(&$data){
    global $zbp;
    
    $db=new Database__MySqli;
    
    if($db->Open(array(
    	$zbp->option['ZC_MYSQL_SERVER1'],
    	$zbp->option['ZC_MYSQL_USERNAME1'],
    	$zbp->option['ZC_MYSQL_PASSWORD1'],
    	$zbp->option['ZC_MYSQL_NAME1'],
    	$zbp->option['ZC_MYSQL_PRE1'],
        $zbp->option['ZC_MYSQL_PORT1'],
        $zbp->option['ZC_MYSQL_PERSISTENT1'],
        $zbp->option['ZC_MYSQL_ENGINE1']
    ))==false){
    	$zbp->ShowError(67,__FILE__,__LINE__);
    }
    
    $LogID=$data['out_trade_no'];    			   	 
    if(empty($LogID)) die();
    $YtUser_buy_Table='zbp_buy';
    $YtUser_buy_DataInfo=array(
        'ID' => array('buy_ID', 'integer', '', 0), 
        'OrderID' => array('buy_OrderID', 'string', 30, 0),   
        'LogID' => array('buy_LogID', 'integer', '', 0), 
        'AuthorID' => array('buy_AuthorID', 'integer', '', 0), 
        'Title' => array('buy_Title', 'string', 255, ''),
        'State' => array('buy_State', 'integer', '', 0), 
        'PostTime' => array('buy_PostTime', 'integer', '', 0),
        'Pay' => array('buy_Pay', 'integer', '', 0), 
        'Express' => array('buy_Express', 'string', '', ''), 
        'IP' => array('buy_IP', 'string', 15, ''),
    ); 

    $sql = $zbp->db->sql->Select($YtUser_buy_Table, array('*'), array(array('=','buy_OrderID',$LogID)), null, null, null);
    $array = $zbp->db->Query($sql);
    $more=json_encode($data);
    
    $dataa = array(
        'send' =>$array['0']['buy_LogID'],
        'uid' =>$array['0']['buy_AuthorID'],
        'price' =>$data['price'],
        'time' =>time(),
		'more' => $more,
        'returnurl' =>$array['0']['buy_Title'],
        'noticeurl' =>$array['0']['buy_Express'],
        'orderid' =>$array['0']['buy_OrderID']
	);
	
	$sql = $db->sql->Insert("cmf_order",$dataa);
    $db->Query($sql);
    $db->Close();
    
    $cdata=[];
    $cdata['date']['orderid']=$dataa['orderid'];
    $cdata['date']['price']=$data['price'];
    $cdata['date']['uid']=$data['uid'];
    $cdata['date']['time']=time();
    $cdata['date']['returnurl']=$dataa['returnurl'];
    $cdata['date']['noticeurl']=$dataa['noticeurl'];
    $cdata['date']['mm']="uco482rH";
    ksort($cdata['date']);
    reset($cdata['date']);
    $sign = '';
    foreach ($cdata['date'] AS $key => $val) {
        if ($val == '' || $key == 'sign') continue; //跳过这些不签名
        if ($sign) $sign .= '&'; //第一个字符串签名不加& 其他加&连接起来参数
        $sign .= "$key=$val";
    }
    $len = str_pad(6 + strlen($sign), 6, "0", STR_PAD_LEFT);
    $sign=$len.$sign;
    $cdata['key']=md5($sign);
    unset($cdata['date']['mm']);
    $c = AppCentre_Get_Cookies();
	$u = AppCentre_Get_UserAgent();
    $ajax->open('POST', $url);
	$ajax->setTimeOuts(120, 120, 0, 0);
	$ajax->setRequestHeader('User-Agent', $u);
	$ajax->setRequestHeader('Cookie', $c);
	$ajax->send($cdata);
	
    return "success";
}


function InstallPlugin_alipay()
{
    global $zbp;
    if (!$zbp->Config('alipay')->HasKey('ver')) {
        $zbp->Config('alipay')->ver = '12';
        $zbp->Config('alipay')->partner = '';
        $zbp->Config('alipay')->key = '';
        $zbp->Config('alipay')->alipayaccount = '';
        $zbp->Config('alipay')->alipayname = '';
        $zbp->Config('alipay')->transport = '';
        $zbp->Config('alipay')->savelogs = 0;
        $zbp->SaveConfig('alipay');
    }
    if ($zbp->Config('alipay')->HasKey('payforname')) {
        $zbp->Config('alipay')->DelKey('payforname');
    }
    if ($zbp->Config('alipay')->HasKey('notify_add')) {
        $zbp->Config('alipay')->DelKey('notify_add');
    }
    $zbp->Config('alipay')->Version = '12';
    $zbp->SaveConfig('alipay');
}
