{* Template Name:列表页单条置顶文章 *}
<li class="post-list clearfix mb15">
    <a href="{$article.Url}" target="_blank"><span class="img-box" data-ratio="16:16"><img src="{tx_freecms_FirstIMG($article,$article,500,400)}" alt="{$article.Title}"></span></a>
    <section>
        <h2 class="f-18 f-blod txt-ov mb5"><a target="_blank" href="{$article.Url}">[置顶] {$article.Title}</a></h2>
        <p class="i40 f-gray mb10">{tx_freecms_intro($article,120,'...')}</p>
        <p class="f-12 f-gray1"><span class="mr15">发布时间：{$article.Time('Y-m-d')}</span><span class="mr15">分类：<a href="{$article.Category.Url}">{$article.Category.Name}</a></span><span class="mr15">浏览：{$article.ViewNums}</span><span class="mr15">评论：{$article.CommNums}</span></p>
    </section>
</li>
<hr class="tx-hr mb15">