{* Template Name:文章页文章内容 *}
<div class="tx-box pd15 mb15">
    <div class="info-title ta-c pd15">
        <h1 class="f-22 f-bold mb10">{$article.Title}</h1>
        <p class="f-12 f-gray1"><span class="mr15">作者：{$article.Author.StaticName}</span><span class="mr15">发布时间：{$article.Time('Y-m-d')}</span><span class="mr15">分类：<a href="{$article.Category.Url}">{$article.Category.Name}</a></span><span class="mr15">浏览：{$article.ViewNums}</span><span class="mr15">评论：{$article.CommNums}</span></p>
    </div>
    <hr class="tx-hr mb15">
    <div class="info-txt pd15 mb15">
        导读：{tx_freecms_intro($article,90,'...')}
    </div>
    <div class="tx-text f-16 mb15">
        {$article.Content}
        <p>标签：{foreach $article.Tags as $tag}<a href="{$tag.Url}" class="mr10">{$tag.Name}</a>{/foreach}</p>
    </div>
    <hr class="tx-hr mb15">
    <div class="info-next">
        <ul class="row">
            <li class="col-12 col-m-24">上一篇：{if $article.Prev}<a  href="{$article.Prev.Url}" title="{$article.Prev.Title}">{$article.Prev.Title}</a>{else}已经是第一篇了{/if}</li>
            <li class="col-12 col-m-24 ta-r">下一篇：{if $article.Next}<a  href="{$article.Next.Url}" title="{$article.Next.Title}">{$article.Next.Title}</a>{else}已经是最后一篇了{/if}</li>
        </ul>
    </div>
</div>

<div class="tx-box mb15 pd15">
    <h2 class="tx-title1 mb15"><strong>相关推荐</strong></h2>
    {if $article.Category.Metas.liststyle=='2'}
    <ul class="row1">
        {php}$relates = GetList(8,null,null,null,null,null,array("is_related" => $article->ID));{/php}
        {if count($relates)>0}
        {foreach $relates as $related}
        <li class="col-6 col-m-12 col1- mb15">
            <a href="{$related.Url}" target="_blank" class="img-post">
                <span class="img-box" data-ratio="16:16"><img src="{tx_freecms_FirstIMG($related,$related,500,400)}" alt="{$related.Title}"></span>
                <p><span class="i40 dp-b">{$related.Title}</span></p>
            </a>
        </li>
        {/foreach}
        {else}
        {foreach GetList(8,$article->Category->ID,null,null,null,null,array('has_subcate'=>true))  as $related}
        <li class="col-6 col-m-12 col1- mb15">
            <a href="{$related.Url}" target="_blank" class="img-post">
                <span class="img-box" data-ratio="16:16"><img src="{tx_freecms_FirstIMG($related,$related,500,400)}" alt="{$related.Title}"></span>
                <p><span class="i40 dp-b">{$related.Title}</span></p>
            </a>
        </li>
        {/foreach}
        {/if}
    </ul>
    {else}
    <ul class="ul-30 ul-arrow row">
        {php}$relates = GetList(10,null,null,null,null,null,array("is_related" => $article->ID));{/php}
        {if count($relates)>0}
        {foreach $relates as $related}
        <li class="col-12 col-m-24"><a href="{$related.Url}" title="{$related.Title}" target="_blank">{$related.Title}</a></li>
        {/foreach}
        {else}
        {foreach GetList(10,$article->Category->ID,null,null,null,null,array('has_subcate'=>true))  as $related}
        <li class="col-12 col-m-24"><a href="{$related.Url}" title="{$related.Title}" target="_blank">{$related.Title}</a></li>
        {/foreach}
        {/if}
    </ul>
    {/if}
</div>

{if !$article.IsLock}
<div class="tx-box mb15 pd15">
    {template:comments}
</div>
{/if}