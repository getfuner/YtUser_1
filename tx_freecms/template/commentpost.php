<div class="tx-comment" id="divCommentPost">
    {if $user.ID>0}
    <h3 class="f-18 mb10"><a name="comment" rel="nofollow" id="cancel-reply" href="#divCommentPost" style="display:none;float:right;"><small>取消回复</small></a>欢迎 <span class="f-red">{if $user.ID>0}{$user.StaticName}{else}你{/if}</span> 发表评论:</h3>
    <ul class="row2">
        <form id="frmSumbit" target="_self" method="post" action="{$article.CommentPostUrl}" class="clearfix">
            <input type="hidden" name="inpId" id="inpId" value="{$article.ID}" />
            <input type="hidden" name="inpRevID" id="inpRevID" value="0" />
            
            <input type="hidden" name="inpName" id="inpName" value="{$user.Name}" />
            <input type="hidden" name="inpEmail" id="inpEmail" value="{$user.Email}" />
            <input type="hidden" name="inpHomePage" id="inpHomePage" value="{$user.HomePage}" />
            {if $option['ZC_COMMENT_VERIFY_ENABLE']}
            <li class="{if $option['ZC_COMMENT_VERIFY_ENABLE']}col-6{else}col-8{/if} col-m-24 col2- mb10">
                <div class="input-ma">
                    <input type="text" name="inpVerify" id="inpVerify" class="tx-input" value="" size="28" tabindex="4" placeholder="验证码(*)">
                    <img src="{$article.ValidCodeUrl}" alt="请填写验证码" onclick="javascript:this.src='{$article.ValidCodeUrl}&amp;tm='+Math.random();">
                </div>
            </li>
            {/if}
            <li class="col-24 col-m-24 col2-">
                <div class="tx-comment-textarea clearfix">
                    <textarea name="txaArticle" id="txaArticle" class="tx-textarea" cols="50" rows="4" tabindex="5" placeholder="欢迎你的交流评论，但是垃圾评论不受欢迎"></textarea>
                    <input name="sumbit" type="submit" tabindex="6" value="提交评论" onclick="return zbp.comment.post()" class="tx-btn tx-bg">
                </div>
            </li>
        </form>
    </ul>
    {else}
    <h3 class="f-18 mb10"> <span class="f-red">登录后即可留言</span></h3>
    {/if}
</div>