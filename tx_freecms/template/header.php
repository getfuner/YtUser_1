{* Template Name:公共头部 *}
<?php include('tx-safe.php');?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="renderer" content="webkit">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
        {if $zbp->Config('tx_freecms')->seoon=='1'} 
        {template:seo} 
        {else}
        <title>{$title}-{$name}</title>
        {/if}
        <meta name="generator" content="{$zblogphp}" />
        <link rel="stylesheet" href="{$host}zb_users/theme/{$theme}/style/css/font-awesome.min.css" type="text/css" media="all" />
        {if $type=='index'&&$page=='1'}
        <link rel="stylesheet" href="{$host}zb_users/theme/{$theme}/style/css/swiper-4.2.2.min.css" type="text/css" media="all" />
        {/if}
        <link rel="stylesheet" href="{$host}zb_users/theme/{$theme}/style/css/shiui.min.css" type="text/css" media="all" />
        <link rel="stylesheet" href="{$host}zb_users/theme/{$theme}/style/{$style}.css" type="text/css" media="all" />
        <script src="{$host}zb_system/script/jquery-2.2.4.min.js" type="text/javascript"></script>
        <script src="{$host}zb_system/script/zblogphp.js" type="text/javascript"></script>
        <script src="{$host}zb_system/script/c_html_js_add.php" type="text/javascript"></script>
        {if $type=='index'&&$page=='1'}
        <script src="{$host}zb_users/theme/{$theme}/script/swiper-4.2.2.min.js"></script>
        {/if}
        {$header}
    </head>
    <body>
        <div class="top f-12 waphide">
            <div class="wide">
                <div class="fr">
                {if $user.Level==6}
                <a href="{$host}?Login">会员登录</a>&nbsp;&nbsp;<a href="{$host}?Register">会员注册</a>
                {else}
                <span >欢迎 {$user.StaticName} 
                {if $user.Level==5}普通用户{/if}
                {if $user.Level==4}VIP用户{/if}
                &nbsp;&nbsp;
                <a href="{$host}?User">会员中心</a>&nbsp;&nbsp;<a href="{$host}?Integral">会员积分（{$user.Price}）</a>
                &nbsp;&nbsp;
                <a href="{BuildSafeCmdURL('act=logout')}">退出</a>
                {/if}
                </div>
                <div class="fl">{if $zbp->Config('tx_freecms')->topl}{$zbp->Config('tx_freecms')->topl}{else}{$subname}{/if}</div>
            </div>
        </div>

        <div class="header">
            <div class="wide">
                <div class="logo fl">
                    {if $type=='article'|$type=='page'}
                    <a href="{$host}" title="{$title}"><img src="{php}tx_freecms_Get_Logo('logo','png');{/php}" alt="{$title}"></a>
                    {else}
                    <h1><a href="{$host}" title="{$title}"><img src="{php}tx_freecms_Get_Logo('logo','png');{/php}" alt="{$title}"></a></h1>
                    {/if}
                </div>
                {if $zbp->Config('tx_freecms')->gg1}
                <div class="logogg fl waphide">
                    {$zbp->Config('tx_freecms')->gg1}
                </div>
                {/if}
                <div class="search fr">
                    <form name="search" method="post" action="{$host}zb_system/cmd.php?act=search" class="fl"><input name="q" size="11" id="edtSearch" type="text" placeholder="请输入关键词"  autocomplete="off"> <button class="search-submit" id="btnPost" type="submit"><i class="fa fa-search"></i></button></form>
                </div>
            </div>
            <a href="javascript:;" class="nav-on pchide"><i class="fa fa-bars"></i></a>
            <a href="javascript:;" class="search-on pchide"><i class="fa fa-search"></i></a>
        </div>

        <div class="nav mb15">
            <ul class="wide">
                {module:navbar}
            </ul>
        </div>
        