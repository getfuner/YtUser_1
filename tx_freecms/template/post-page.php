{* Template Name:页面内容 *}
<div class="tx-box pd15 mb15">
    <div class="info-title ta-c pd15">
        <h1 class="f-22 f-bold mb5">{$article.Title}</h1>
        <p class="f-12 f-gray1"><span class="mr15">作者：{$article.Author.StaticName}</span><span class="mr15">发布时间：{$article.Time('Y-m-d')}</span><span class="mr15">浏览：{$article.ViewNums}</span><span class="mr15">评论：{$article.CommNums}</span></p>
    </div>
    <hr class="tx-hr mb15">
    <div class="tx-text f-16">
        {$article.Content}
    </div>
</div>


{if !$article.IsLock}
<div class="tx-box mb15 pd15">
    {template:comments}
</div>
{/if}