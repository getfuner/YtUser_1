{* Template Name:所有评论模板 *}
{if $socialcomment}
{$socialcomment}
{else}

{if $article.CommNums > 0}
<div class="tx-comments mb15  bg-white">
    <h3 class="f-18 mb10">已有{$article.CommNums}位网友发表了看法：</h3>
    <label id="AjaxCommentBegin"></label>
    <!--评论输出-->
    {foreach $comments as $key => $comment}
    {template:comment}
    {/foreach}
    
    {if $article.CommNums>$zbp->Config('system')->ZC_COMMENTS_DISPLAY_COUNT}
    <!--评论翻页条输出-->
    <div class="pagebar commentpagebar pd10">
        {template:pagebar}
    </div>
    {/if}
    <label id="AjaxCommentEnd"></label>
</div>
{/if}
<!--评论框-->
{template:commentpost}

{/if}