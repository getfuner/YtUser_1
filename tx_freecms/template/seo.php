{* Template Name: 主题自带seo优化模板 *}
{if $type=='article'}
<title>{if $article.Metas.titles}{$article.Metas.titles}{else}{$title}{tx_freecms_connector()}{$article.Category.Name}{tx_freecms_connector()}{$name}{/if}</title>
<meta name="keywords" content="{if $article.Metas.keywords}{$article.Metas.keywords}{else}{foreach $article.Tags as $tag}{$tag.Name},{/foreach}{$name}{/if}" />
<meta name="description" content="{if $article.Metas.description}{$article.Metas.description}{else}{tx_freecms_intro($article,130,'')},{$name}{/if}" />
{elseif $type=='page'}
<title>{if $article.Metas.titles}{$article.Metas.titles}{else}{$title}{tx_freecms_connector()}{$name}{/if}</title>
<meta name="keywords" content="{if $article.Metas.keywords}{$article.Metas.keywords}{else}{$title}{/if}" />
<meta name="description" content="{if $article.Metas.description}{$article.Metas.description}{else}{tx_freecms_intro($article,130,'')},{$name}{/if}" />
<meta name="author" content="{$article.Author.StaticName}">
{elseif $type=='index'}
<title>{if $zbp->Config('tx_freecms')->titles}{$zbp->Config('tx_freecms')->titles}{if $page>'1'}{tx_freecms_connector()}第{$pagebar.PageNow}页{/if}{else}{$subname}{if $page>'1'}{tx_freecms_connector()}第{$pagebar.PageNow}页{/if}{tx_freecms_connector()}{$name}{/if}</title>
<meta name="Keywords" content="{$zbp->Config('tx_freecms')->keywords}">
<meta name="description" content="{$zbp->Config('tx_freecms')->description}">
{elseif $type=='category'}
<title>{if $category.Metas.titles}{$category.Metas.titles}{else}{$title}{tx_freecms_connector()}{$name}{if $page>'1'}{tx_freecms_connector()}第{$pagebar.PageNow}页{/if}{/if}</title>
<meta name="Keywords" content="{if $category.Metas.keywords}{$category.Metas.keywords}{else}{$title},{$name}{/if}">
<meta name="description" content="{if $category.Intro}{$category.Intro}{else}{$title},{$name}{/if}">
{elseif $type=='tag'}
<title>{if $tag.Metas.titles}{$tag.Metas.titles}{else}{$title}{tx_freecms_connector()}{$name}{if $page>'1'}{tx_freecms_connector()}第{$pagebar.PageNow}页{/if}{/if}</title>
<meta name="Keywords" content="{if $tag.Metas.keywords}{$tag.Metas.keywords}{else}{$title},{$name}{/if}">
<meta name="description" content="{if $tag.Intro}{$tag.Intro}{else}{$title},{$name}{/if}">
{else}
<title>{$title}{tx_freecms_connector()}{$name}</title>
{/if}