{* Template Name:首页及列表页 *}
{template:header}
{if $type=='index'&&$page=='1'}
<div class="main wide">
    {if $zbp->Config('tx_freecms')->gg2}
    <div class="gg-box mb15">
        {$zbp->Config('tx_freecms')->gg2}
    </div>
    {/if}
    <div class="row1">
        <div class="col-17 col-m-24 col1- mb15">
            <div class="tx-box pd15-3">
                <div class="row">
                    <div class="col-10 col-m-24 mb15">
                        <div class="index-flash"> 
                            <div class="swiper-container"> 
                                <div class="swiper-wrapper"> 
                                    {if $zbp->Config('tx_freecms')->flashon=='1'}
                                    {$zbp->Config('tx_freecms')->flash}
                                    {else}
                                    {foreach $array=tx_freecms_hot_list(3,300) as $related}
                                    <div class="swiper-slide"><a href="{$related.Url}" target="_blank"><img src="{tx_freecms_FirstIMG($related,$related,500,400)}" alt="{$related.Title}"><p>{$related.Title}</p></a></div> 
                                    {/foreach}
                                    {/if}
                                </div> 
                                <div class="swiper-pagination"></div>
                            </div> 
                            <script>
                                var swiper = new Swiper('.index-flash .swiper-container', {
                                    spaceBetween: 30,
                                    centeredSlides: true,
                                    loop: true,
                                    autoHeight: true,
                                    autoplay: {
                                        delay: 5000,
                                        disableOnInteraction: false,
                                    },
                                    pagination: {
                                        el: '.swiper-pagination',
                                        clickable: true,
                                    },
                                });
                            </script> 
                        </div>
                    </div>
                    <div class="col-14 col-m-24 mb15">
                        {$topArray = GetList(1, null, null, null, null, null, array("only_ontop"  => true));}
                        {if count($topArray)>0}
                        {foreach $topArray as $related}
                        <h2 class="f-18 f-bold txt-ov mb5"><a target="_blank" href="{$related.Url}" class="f-black">{$related.Title}</a></h2>
                        <p class="i40 f-12 f-gray mb15">{tx_freecms_intro($related,80,'...')}<a target="_blank" href="{$related.Url}" class="f-blue">[查看详情]</a></p>
                        {/foreach}
                        {else}
                        {foreach $array=tx_freecms_hot_list(1,300) as $related}
                        <h2 class="f-18 f-bold txt-ov mb5"><a target="_blank" href="{$related.Url}" class="f-black">{$related.Title}</a></h2>
                        <p class="i40 f-12 f-gray mb15">{tx_freecms_intro($related,80,'...')}<a target="_blank" href="{$related.Url}" class="f-blue">[查看详情]</a></p>
                        {/foreach}
                        {/if}
                        <hr class="tx-hr mb15">
                        <ul class="ul-33 ul-spot">
                            {foreach GetList(6) as $related}
                            <li><span class="fr f-gray">{$related.Time('m-d')}</span><a href="{$related.Url}" title="{$related.Title}" target="_blank">{$related.Title}</a></li>
                            {/foreach}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-7 col-m-24 col1- mb15">
            <div class="tx-box pd15">
                <div class="tx-title mb10">
                    <strong>排行榜</strong>
                    <ul class="tab-hd fr clearfix f-bold f-16">
                        <li class="mr10 on">月排行</li>
                        <li>年排行</li>
                    </ul>
                </div>
                <div class="tab-bd">
                    <ul class="ul-33 ul-rank">
                        {foreach $array=tx_freecms_hot_list(8,30) as $key=>$related}
                        {$i=$key}
                        <li><span>{$i+1}</span><a href="{$related.Url}" title="{$related.Title}" target="_blank">{$related.Title}</a></li>
                        {/foreach}
                    </ul>

                    <ul class="ul-33 ul-rank hide">
                        {foreach $array=tx_freecms_hot_list(8,365) as $key=>$related}
                        {$i=$key}
                        <li><span>{$i+1}</span><a href="{$related.Url}" title="{$related.Title}" target="_blank">{$related.Title}</a></li>
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="row1">
        {if $zbp->Config('tx_freecms')->cms}
        {php}$flids = explode(',',$zbp->Config('tx_freecms')->cms);{/php}
        {foreach $flids as $flid}
        {if isset($categorys[$flid])}
        {if $categorys[$flid]->Metas->liststyle=='1'}
        <div class="col-24 col-m-24 col1- mb15">
            <div class="tx-box pd15-3 clearfix">
                <div class="tx-title mb15"><a href="{$categorys[$flid].Url}" target="_blank" class="fr f-12 f-gray">更多 ></a><strong>{$categorys[$flid].Name}</strong></div>
                <ul class="row1 txt-post mb5 ta-c">
                    {foreach GetList(30,$flid,null,null,null,null,array('has_subcate'=>true))  as $related}
                    <li class="col-4 col-m-24 col1- mb10"><a href="{$related.Url}" title="{$related.Title}" target="_blank">{$related.Title}</a></li>
                    {/foreach}
                </ul>
            </div>
        </div>
        {elseif $categorys[$flid]->Metas->liststyle=='2'}
        <div class="col-24 col-m-24 col1- mb15">
            <div class="tx-box pd15-3">
                <div class="tx-title mb15"><a href="{$categorys[$flid].Url}" target="_blank" class="fr f-12 f-gray">更多 ></a><strong>{$categorys[$flid].Name}</strong></div>
                <ul class="row1">
                    {foreach GetList(6,$flid,null,null,null,null,array('has_subcate'=>true))  as $related}
                    <li class="col-4 col-m-12 col1- mb15">
                        <a href="{$related.Url}" target="_blank" class="img-post">
                            <span class="img-box" data-ratio="16:16"><img src="{tx_freecms_FirstIMG($related,$related,500,400)}" alt="{$related.Title}"></span>
                            <p><span class="i40 dp-b">{$related.Title}</span></p>
                        </a>
                    </li>
                    {/foreach}
                </ul>
            </div>
        </div>
        {else}
        <div class="col-8 col-m-24 col1- mb15">
            <div class="tx-box pd15">
                <div class="tx-title mb15"><a href="{$categorys[$flid].Url}" target="_blank" class="fr f-12 f-gray">更多 ></a><strong>{$categorys[$flid].Name}</strong></div>
                <ul class="ul-33 ul-arrow">
                    {foreach GetList(8,$flid,null,null,null,null,array('has_subcate'=>true))  as $related}
                    <li><a href="{$related.Url}" title="{$related.Title}" target="_blank">{$related.Title}</a></li>
                    {/foreach}
                </ul>
            </div>
        </div>
        {/if}
        {/if}
        {/foreach}
        {/if}
    </div>
    
    <div class="tx-box pd15 mb15">
        <div class="tx-title mb15"><strong>{$modules['link'].Name}</strong></div>
        <ul class="links clearfix">
            {module:link}
        </ul>
    </div>
    
    {if $zbp->Config('tx_freecms')->gg3}
    <div class="gg-box mb15">
        {$zbp->Config('tx_freecms')->gg3}
    </div>
    {/if}
    
</div>

{else}

<div class="main wide">
    {if $zbp->Config('tx_freecms')->gg2}
    <div class="gg-box mb15">
        {$zbp->Config('tx_freecms')->gg2}
    </div>
    {/if}
    <div class="place mb15 f-12">
        当前位置：<a href="{$host}">网站首页</a>{if $type=='category'}
        {php}
        $html='';
        function navcate($id){
        global $html;
        $cate = new Category;
        $cate->LoadInfoByID($id);
        $html =' > <a href="' .$cate->Url.'" title="查看' .$cate->Name. '中的全部文章">' .$cate->Name. '</a> '.$html;
        if(($cate->ParentID)>0){navcate($cate->ParentID);}
        }
        navcate($category->ID);
        global $html;
        echo $html;
        {/php}

        {else} > {$title}{/if}
    </div>
    {if $type=='category'}
    <div class="tx-box pd15-3 mb15">
        <div class="tx-title1 mb15"><strong>{$category.Name}精选</strong></div>
        <ul class="row1">
            {$topArray = GetList(6, $category->ID, null, null, null, null, array("only_ontop"  => true));}
            {if count($topArray)>0}
            {foreach $topArray as $related}
            <li class="col-4 col-m-12 col1- mb15">
                <a href="{$related.Url}" target="_blank" class="img-post1">
                    <span class="img-box mb5" data-ratio="16:16"><img src="{tx_freecms_FirstIMG($related,$related,500,400)}" alt="{$related.Title}"></span>
                    <p>{$related.Title}</p>
                </a>
            </li>
            {/foreach}
            {else}
            {foreach $array=tx_freecms_hot_GetArticleCategorys(6,$category->ID,365,true) as $related}
            <li class="col-4 col-m-12 col1- mb15">
                <a href="{$related.Url}" target="_blank" class="img-post1">
                    <span class="img-box mb5" data-ratio="16:16"><img src="{tx_freecms_FirstIMG($related,$related,500,400)}" alt="{$related.Title}"></span>
                    <p>{$related.Title}</p>
                </a>
            </li>
            {/foreach}
            {/if}
        </ul>
    </div>
    {/if}
    <div class="row1">
        <div class="col-17 col-m-24 col1- mb15">
            <div class="tx-box pd15">
                <div class="tx-title1 mb15"><strong>{if $type=='category'}{$category.Name}列表{else}{$title}{/if}</strong></div>
                
                {if $type=='category'}
                {if $category.Metas.liststyle=='1'}
                <ul class="ul-36 ul-arrow ul-line mb15">
                    {foreach $articles as $article}
                    {if $article.IsTop}
                    {template:post-multi-txt}
                    {else}
                    {template:post-multi-txt}
                    {/if}
                    {/foreach}
                </ul>
                {elseif $category.Metas.liststyle=='2'}
                <ul class="row1">
                    {foreach $articles as $article}
                    {if $article.IsTop}
                    {template:post-multi-img}
                    {else}
                    {template:post-multi-img}
                    {/if}
                    {/foreach}
                </ul>
                {else}
                <ul class="clearfix">
                    {foreach $articles as $article}
                    {if $article.IsTop}
                    {template:post-istop}
                    {else}
                    {template:post-multi}
                    {/if}
                    {/foreach}
                </ul>
                {/if}
                {else}
                <ul class="clearfix">
                    {foreach $articles as $article}
                    {if $article.IsTop}
                    {template:post-istop}
                    {else}
                    {template:post-multi}
                    {/if}
                    {/foreach}
                </ul>
                {/if}
                <div class="pagebar ta-c">
                    {template:pagebar}
                </div>
            </div>
        </div>


        <div class="tx-side col-7 col-m-24 col1-">
            {if $type=='category'}
            <dl>
                <dt>{$category.Name}排行</dt>
                <dd>
                    <ul class="ul-33 ul-rank">
                        {foreach $array=tx_freecms_hot_GetArticleCategorys(9,$category->ID,365,true) as $key=>$related}
                        {$i=$key}
                        <li><span>{$i+1}</span><a href="{$related.Url}" title="{$related.Title}" target="_blank">{$related.Title}</a></li>
                        {/foreach}
                    </ul>
                </dd>
            </dl>
            {/if}
            
            {template:sidebar2}
        </div>
    </div>
    
    {if $zbp->Config('tx_freecms')->gg3}
    <div class="gg-box mb15">
        {$zbp->Config('tx_freecms')->gg3}
    </div>
    {/if}
</div>

{/if}

{template:footer}