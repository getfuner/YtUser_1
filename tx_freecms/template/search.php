{* Template Name:搜索页 *}
{template:header}
<div class="main wide">
    <div class="place mb15 f-12">
        当前位置：<a href="{$host}">网站首页</a> > {$title}
    </div>

    <div class="row1">
        <div class="col-17 col-m-24 col1- mb15">
            <div class="tx-box pd15">
                <div class="tx-title1 mb15"><strong>{$title}</strong></div>
                <ul class="clearfix">
                    {foreach $articles as $article}
                    {if $article.IsTop}
                    {template:post-istop}
                    {else}
                    {template:post-multi}
                    {/if}
                    {/foreach}
                </ul>
                <div class="pagebar ta-c">
                    {template:pagebar}
                </div>
            </div>
        </div>


        <div class="tx-side col-7 col-m-24 col1-">
            {template:sidebar2}
        </div>
    </div>
</div>

{template:footer}