{* Template Name:文章页单页 *}
{template:header}
<div class="main wide">
    {if $zbp->Config('tx_freecms')->gg2}
    <div class="gg-box mb15">
        {$zbp->Config('tx_freecms')->gg2}
    </div>
    {/if}
    <div class="place mb15 f-12">
        当前位置：<a href="{$host}">网站首页</a>{if $type=='article'}
        {php}
        $html='';
        function navcate($id){
        global $html;
        $cate = new Category;
        $cate->LoadInfoByID($id);
        $html =' > <a href="' .$cate->Url.'" title="查看' .$cate->Name. '中的全部文章">' .$cate->Name. '</a> '.$html;
        if(($cate->ParentID)>0){navcate($cate->ParentID);}
        }
        navcate($article->Category->ID);
        global $html;
        echo $html;
        {/php} > 正文
        {else} > {$title}{/if}
    </div>
    <div class="row1">
        <div class="col-17 col-m-24 col1-">
            {if $article.Type==ZC_POST_TYPE_ARTICLE}
            {template:post-single}
            {else}
            {template:post-page}
            {/if}
        </div>

        <div class="tx-side col-7 col-m-24 yt-side">
            {if $type=='article'}
            {if $article.Metas.price >0}
            <dl>
                <dd>

                {if $article.Buypay=="0"}
                    {if $zbp->Config('YtUser')->isvipdownload == 1 && $user->Level <= 4}
                        <div class="item price"><span>VIP免费下载</span></div>
                        <a href="{$host}?download={$article.ID}" target="_blank" class="tx-btn yt-bug">点击下载</a>
                        {if $article.Metas.xzmm}
                            <div class="tips">网盘密码：{$article.Metas.xzmm}</div>
                        {/if}
                    {else}
                        <input type="hidden" name="LogID" id="LogID" value="{$article.ID}" />
                        <div class="item price"><span>{$article->Metas->price}</span> 积分</div>
                        <input name="sumbit" type="submit" tabindex="6" value="立即购买" onclick="return YtSbuy()" class="tx-btn yt-bug">
                        <div class="tips">
                            {$zbp->Config('YtUser')->download_notice}
                            <br>
                        </div>
                    {/if}
                {else}
                    <div class="item price"><span>你已购买</span></div>
                    <a href="{$host}?download={$article.ID}" target="_blank" class="tx-btn yt-bug">点击下载</a>
                    {if $article.Metas.xzmm}
                    <div class="tips">网盘密码：{$article.Metas.xzmm}</div>
                    {/if}
                {/if}
                </dd>
            </dl>
            {/if}
            {/if}

            {if $type=='article'}
            <dl>
                <dt>{$article.Category.Name}排行</dt>
                <dd>
                    <ul class="ul-33 ul-rank">
                        {foreach $array=tx_freecms_hot_GetArticleCategorys(9,$article->Category->ID,365,true) as $key=>$related}
                        {$i=$key}
                        <li><span>{$i+1}</span><a href="{$related.Url}" title="{$related.Title}" target="_blank">{$related.Title}</a></li>
                        {/foreach}
                    </ul>
                </dd>
            </dl>
            {/if}

            {template:sidebar2}
        </div>
    </div>
    
    {if $zbp->Config('tx_freecms')->gg3}
    <div class="gg-box mb15">
        {$zbp->Config('tx_freecms')->gg3}
    </div>
    {/if}
</div>
{template:footer}