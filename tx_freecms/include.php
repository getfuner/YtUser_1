<?php
RegisterPlugin("tx_freecms","ActivePlugin_tx_freecms");


function ActivePlugin_tx_freecms(){
    Add_Filter_Plugin('Filter_Plugin_Admin_TopMenu','tx_freecms_AddMenu');
    Add_Filter_Plugin('Filter_Plugin_Edit_Response5','tx_freecms_Filter_Plugin_Edit_Response5');
    Add_Filter_Plugin('Filter_Plugin_Category_Edit_Response','tx_freecms_Category_Edit_Response');
    Add_Filter_Plugin('Filter_Plugin_Tag_Edit_Response','tx_freecms_Tag_Edit_Response');
}

define( 'tx_freecms_THIS','tx_freecms');
define( 'tx_freecms_ROOT_DIR',plugin_dir_path(tx_freecms_THIS));
define( 'tx_freecms_ROOT_URL',plugin_dir_url(tx_freecms_THIS));
function tx_freecms_Get_Logo($name,$type){ 
    $path = tx_freecms_ROOT_DIR.'tx_freecms/include/'.$name.'.'.$type;
    if (file_exists($path)){
        echo tx_freecms_ROOT_URL.'tx_freecms/include/'.$name.'.'.$type;
    }else{
        echo tx_freecms_ROOT_URL.'tx_freecms/include/'.$name.'_tx.'.$type;
    }
}

function tx_freecms_Thumbs($src,$width,$height) {
    global $zbp;
    if (!$zbp->CheckPlugin('IMAGE')) {
        $thumbs_src=$src;
    } else {
        $thumbs_src=IMAGE::getPicUrlBy($src,$width,$height,4);
    }
    return $thumbs_src;
}

function tx_freecms_FirstIMG($as,$pos,$width,$height) {
    global $zbp;   
    $temp=mt_rand(1,3);
    $pattern = "/<img.*?src=(\"|')?(?<src>.*?\.(gif|jpg|jpeg|png))(\"|')?.*?>/";   
    $content = $as->Content;
    if ($pos->Metas->thumbnail){
        $temp=$pos->Metas->thumbnail;
    }else{
        if(preg_match($pattern,$content,$matchContent) && isset($matchContent['src'])){
            $temp=$matchContent['src'];
        } else {
            $temp=tx_freecms_Get_Logo('pic','png');
        }
    }
    $src =tx_freecms_Thumbs($temp,$width,$height);
    return $src;
}

function tx_freecms_intro($as,$long,$other){      	 			 
    global $zbp;    		  		  
    $str = '';
    if ($zbp->Config('tx_freecms')->introon == '1'){
        $str = $as->Intro;
    }else{
        if ($as->Metas->description){
            $str  =$as->Metas->description;
        } else {      						
            $str .= preg_replace('/[\r\n\s]+/', '', trim(SubStrUTF8(TransferHTML($as->Content,'[nohtml]'),$long)).$other);     		 	 	 
        } 
    }
    return $str;      	  	  
}  

function tx_freecms_connector(){      	 			 
    global $zbp;    		  		  
    $str = '';     		   	 
    if ($zbp->Config('tx_freecms')->connector){
        $str  =$zbp->Config('tx_freecms')->connector;
    } else {      						
        $str .= '-';     		 	 	 
    }    			  		 
    return $str;      	  	  
}  

function tx_freecms_new_GetArticleCategorys($Rows,$CategoryID,$hassubcate){
    global $zbp;
    $ids = strpos($CategoryID,',') !== false ? explode(',',$CategoryID) : array($CategoryID);
    $wherearray=array(); 
    foreach ($ids as $cateid){
        if (!$hassubcate) {
            $wherearray[]=array('log_CateID',$cateid); 
        }else{
            $wherearray[] = array('log_CateID', $cateid);
            foreach ($zbp->categorys[$cateid]->SubCategorys as $subcate) {
                $wherearray[] = array('log_CateID', $subcate->ID);
            }
        }
    }
    $where=array( 
        array('array',$wherearray), 
        array('=','log_Status','0'), 
    ); 

    $order = array('log_PostTime'=>'DESC'); 
    $articles=    $zbp->GetArticleList(array('*'),$where,$order,array($Rows),'');     

    return $articles;
}

function tx_freecms_hot_GetArticleCategorys($Rows,$CategoryID,$Times,$hassubcate){
    global $zbp;
    $ids = strpos($CategoryID,',') !== false ? explode(',',$CategoryID) : array($CategoryID);
    $wherearray=array(); 
    foreach ($ids as $cateid){
        if (!$hassubcate) {
            $wherearray[]=array('log_CateID',$cateid); 
        }else{
            $wherearray[] = array('log_CateID', $cateid);
            foreach ($zbp->categorys[$cateid]->SubCategorys as $subcate) {
                $wherearray[] = array('log_CateID', $subcate->ID);
            }
        }
    }
    $stime = time();
    $ytime = $Times*24*60*60;
    $ztime = $stime-$ytime;
    $where=array( 
        array('array',$wherearray), 
        array('=','log_Status','0'), 
        array('>','log_PostTime',$ztime), 
    ); 
    $order = array('log_ViewNums'=>'DESC'); 
    $articles=    $zbp->GetArticleList(array('*'),$where,$order,array($Rows),'');     
    return $articles;
}

function tx_freecms_hot_list($Rows,$Times){
    global $zbp;
    $stime = time();
    $ytime = $Times*24*60*60;
    $ztime = $stime-$ytime;
    $order = array('log_ViewNums'=>'DESC');
    $where = array(array('=','log_Status','0'),array('>','log_PostTime',$ztime));
    $articles = $zbp->GetArticleList(array('*'),$where,$order,array($Rows),'');
    if (count($articles)==0){
        $articles = $zbp->GetArticleList(array('*'),array(),array("rand()"),array($Rows),'');
    }
    return $articles;
}


function tx_freecms_Filter_Plugin_Edit_Response5(){
    global $zbp,$article;
    echo '
    <script src="'.$zbp->host.'zb_users/theme/tx_freecms/script/lib.upload.js" type="text/javascript"></script>
    <style>.uploadimg,.files{border:1px solid #ddd;border-radius: 3px;line-height: 38px;height: 38px;padding:0 100px 0 10px;position:relative;margin-bottom: 8px;width: 99%;}.uploadimg input[type="text"],.files input[type="text"]{border:0;width:100%;color:#999;}.uploadimg input[type="button"],.files input[type="button"]{position:absolute;right:3px;top:3px;width:80px;font-size:12px;text-align:center;color:#fff;background-color:#3A6EA5;line-height:30px;height:30px;border:0;cursor:pointer;margin:0;}.mb10{margin-bottom:10px;}#template{display:none}</style>
    ';
    
    echo '<div class="uploadimg"><input name="meta_thumbnail" type="text" class="uplod_img" value="'.$article->Metas->thumbnail.'" placeholder="自定义缩略图"/><input type="button"  id="updatapic2" class="button" value="点击上传"/></div>';
    echo '<table width="99%" class="mb10">
	<tr>
	<td width="15%">SEO标题</td>
	<td width="35%"><input style="width:100%" name="meta_titles" value="'.$article->Metas->titles.'"/></td>
	<td width="15%">SEO关键词</td>	
	<td width="35%"><input style="width:100%" name="meta_keywords" value="'.$article->Metas->keywords.'"/></td>
	</tr>
    <tr>
    <td>SEO描述</td>
    <td colspan="3"><textarea name="meta_description" type="text" id="meta_description" style="width:100%;height:40px;">'.$article->Metas->description.'</textarea></td>
    </tr>
    </table>
	';
}

function tx_freecms_Category_Edit_Response() {
    global $zbp,$cate;
    echo '<style>.tx-box{border: 1px solid #ED0027;border-radius: 3px;margin-bottom: 15px;}.tx-box>h2{font-size:1.1em;font-weight:700;line-height:2.5;padding-left:10px;background-color: #ED0027;color:#fff;margin:0;}.tx-box img{max-width:100%; height:auto; width:auto\9; border:0;vertical-align:middle;}.tx_ul>li {border-bottom: 1px solid #eee;padding: 12px;}.tx_ul li:last-child{border-bottom: 0;}.tx_ul li span.title{width:180px;margin:0 10px 0 0;text-align: right;display: inline-block;}input[type="submit"]{background-color:#ed0027;margin: 0;line-height: 40px;padding: 0 50px 0 50px;height: 40px;border: 0;border-radius: 2px;}</style>';
    echo '
    <div class="tx-box">
        <h2>天兴工作室主题自带seo功能</h2>
        <ul class="tx_ul">
            <li><span class="title">标题</span><input style="width:400px" name="meta_titles" value="'.$cate->Metas->titles.'"/></li>
            <li><span class="title">关键词</span><input style="width:400px" name="meta_keywords" value="'.$cate->Metas->keywords.'"/></li>
            <li><span class="title"><font color="red">此分类显示样式</font></span><span style="margin-right:20px;"><input type="radio" name="meta_liststyle" value="8"> 默认样式(资讯)</span><span style="margin-right:20px;"><input type="radio" name="meta_liststyle" value="1"> 纯文字</span>  <span style="margin-right:20px;"><input type="radio" name="meta_liststyle" value="2">  图片</span></li>
        </ul>
    </div>
    <script>
    var y = "'.$cate->Metas->liststyle.'";
    var radiovar = document.getElementsByName("meta_liststyle");
    for(var i=0;i<radiovar.length;i++){
    if(radiovar[i].value==y)
    radiovar[i].checked = "checked";
    }
    </script>
	';
}

function tx_freecms_Tag_Edit_Response() {
    global $zbp,$tag;
    echo '<style>.tx-box{border: 1px solid #ED0027;border-radius: 3px;margin-bottom: 15px;}.tx-box>h2{font-size:1.1em;font-weight:700;line-height:2.5;padding-left:10px;background-color: #ED0027;color:#fff;margin:0;}.tx-box img{max-width:100%; height:auto; width:auto\9; border:0;vertical-align:middle;}.tx_ul>li {border-bottom: 1px solid #eee;padding: 12px;}.tx_ul li:last-child{border-bottom: 0;}.tx_ul li span.title{width:180px;margin:0 10px 0 0;text-align: right;display: inline-block;}input[type="submit"]{background-color:#ed0027;margin: 0;line-height: 40px;padding: 0 50px 0 50px;height: 40px;border: 0;border-radius: 2px;}</style>';
    echo '
    <div class="tx-box">
        <h2>天兴工作室主题自带seo功能</h2>
        <ul class="tx_ul">
            <li><span class="title">标题</span><input style="width:400px" name="meta_titles" value="'.$tag->Metas->titles.'"/></li>
            <li><span class="title">关键词</span><input style="width:400px" name="meta_keywords" value="'.$tag->Metas->keywords.'"/></li>
        </ul>
    </div>
	';
}

function tx_freecms_SubMenu($id){
    $arySubMenu = array(
        0 => array('主题说明', 'config', 'left', false),
        1 => array('图片上传', 'logo', 'left', false),
        2 => array('SEO设置', 'seo', 'left', false),
        3 => array('主要设置', 'home', 'left', false),
    );
    foreach($arySubMenu as $k => $v){
        echo '<a href="?act='.$v[1].'" '.($v[3]==true?'target="_blank"':'').'><span class="m-'.$v[2].' '.($id==$v[1]?'m-now':'').'">'.$v[0].'</span></a>';
    }
}

function tx_freecms_AddMenu(&$m){
    global $zbp;
    array_unshift($m, MakeTopMenu("root",'主题配置',$zbp->host . "zb_users/theme/tx_freecms/main.php?act=config","","topmenu_tx_freecms"));
}


function InstallPlugin_tx_freecms(){
    global $zbp;
    if(!$zbp->Config('tx_freecms')->HasKey('Version')){
        $zbp->Config('tx_freecms')->Version = '1.0';
        $zbp->Config('tx_freecms')->description = '网站描述';
        $zbp->Config('tx_freecms')->keywords = '网站关键词';
        $zbp->Config('tx_freecms')->seoon = '1';
		$zbp->Config('tx_freecms')->titles = '';
		$zbp->Config('tx_freecms')->connector = '';
		$zbp->Config('tx_freecms')->configureon = '0';
        $zbp->Config('tx_freecms')->introon = '0';
        $zbp->Config('tx_freecms')->topr = '右侧';
        $zbp->Config('tx_freecms')->topl = '';
        $zbp->Config('tx_freecms')->gg1 = '<a href="#"><img src="{#ZC_BLOG_HOST#}zb_users/theme/tx_freecms/style/img/zss.jpg"></a>';
        $zbp->Config('tx_freecms')->gg2 = '<a href="#"><img src="{#ZC_BLOG_HOST#}zb_users/theme/tx_freecms/style/img/zss.jpg"></a>';
        $zbp->Config('tx_freecms')->gg3 = '<a href="#"><img src="{#ZC_BLOG_HOST#}zb_users/theme/tx_freecms/style/img/zss.jpg"></a>';
        $zbp->Config('tx_freecms')->flash = '
<div class="swiper-slide"><a href="#"><img src="{#ZC_BLOG_HOST#}zb_users/theme/tx_freecms/style/img/flash1.jpg"><p>自定义标题1</p></a></div>
<div class="swiper-slide"><a href="#"><img src="{#ZC_BLOG_HOST#}zb_users/theme/tx_freecms/style/img/flash1.jpg"><p>自定义标题2</p></a></div>
        ';
        $zbp->Config('tx_freecms')->flashon = '1';
        $zbp->Config('tx_freecms')->cms = '1,1,1';
        $zbp->SaveConfig('tx_freecms');
    }
}

//卸载主题
function UninstallPlugin_tx_freecms(){
    global $zbp;
	if ($zbp->Config('tx_freecms')->configureon == '1'){
		$zbp->DelConfig('tx_freecms');  
	} 
}
?>
