<?php
require '../../../zb_system/function/c_system_base.php';
require '../../../zb_system/function/c_system_admin.php';

$zbp->Load();
$action='root';
if (!$zbp->CheckRights($action)) {$zbp->ShowError(6);die();}
if (!$zbp->CheckPlugin('tx_freecms')) {$zbp->ShowError(48);die();}
$act = "";
if ($_GET['act']){
    $act = $_GET['act'] == "" ? 'config' : $_GET['act'];
}

$blogtitle='主题配置';

require $blogpath . 'zb_system/admin/admin_header.php';
require $blogpath . 'zb_system/admin/admin_top.php';

if(isset($_POST['description'])){
    $zbp->Config('tx_freecms')->description = $_POST['description'];
    $zbp->Config('tx_freecms')->keywords = $_POST['keywords'];
    $zbp->Config('tx_freecms')->titles = $_POST['titles'];
	$zbp->Config('tx_freecms')->connector = $_POST['connector'];
    $zbp->Config('tx_freecms')->seoon = $_POST['seoon'];
	$zbp->Config('tx_freecms')->configureon = $_POST['configureon'];
    $zbp->Config('tx_freecms')->introon = $_POST['introon'];
    $zbp->SaveConfig('tx_freecms');
    $zbp->ShowHint('good');
}

if(isset($_POST['topr'])){
    $zbp->Config('tx_freecms')->topr = $_POST['topr'];
    $zbp->Config('tx_freecms')->topl = $_POST['topl'];
    $zbp->Config('tx_freecms')->gg1 = $_POST['gg1'];
    $zbp->Config('tx_freecms')->flash = $_POST['flash'];
    $zbp->Config('tx_freecms')->flashon = $_POST['flashon'];
    $zbp->Config('tx_freecms')->cms = $_POST['cms'];
    $zbp->Config('tx_freecms')->gg2 = $_POST['gg2'];
    $zbp->Config('tx_freecms')->gg3 = $_POST['gg3'];
    $zbp->SaveConfig('tx_freecms');
    $zbp->ShowHint('good');
}
?>
<style type="text/css">.tx-box{border: 1px solid #ED0027;border-radius: 3px;padding: 5px 15px;margin-bottom: 15px;}.tx_ul li {border-bottom: 1px solid #eee;padding: 12px 0;}.tx_ul li:last-child{border-bottom: 0;}</style>
<div id="divMain">
    <div class="divHeader"><?php echo $blogtitle;?></div>
    <div class="SubMenu">
        <?php tx_freecms_SubMenu($act);?>
        <a href="https://www.txcstx.cn/muban/" target="_blank"><span class="m-left" style="color:#F00">更多主题</span></a>
    </div>
    
    <div id="divMain2">
        <?php
        if ($act == 'config'){        	 	 
        ?>
        <div class="tx-box">
            <ul class="tx_ul">
                <li>关于侧栏：本主题的侧栏调用是侧栏2，请去<a href="<?php echo $zbp->host;?>zb_system/admin/index.php?act=ModuleMng">模块管理</a>里面自行设置侧栏内容</li>
                <li>原主题作者：<a href="https://www.txcstx.cn/" target="_blank">天兴工作室</a>；网址：<a href="https://www.txcstx.cn/" target="_blank">https://www.txcstx.cn</a>；联系QQ：<a target="_blank" href="http://wpa.qq.com/msgrd?v=3&amp;uin=1109856918&amp;site=qq&amp;menu=yes" rel="nofollow">1109856918</a></li>
                <li>现主题版本由唐朝二次开发，与原主题作者无关，售后请不要找他</li>
                <li>演示站点:<a href="https://zbp.ytecn.com/" target="_blank">豫唐网络下载站</a></li>
            </ul>
        </div>
        <?php
        }    	 	  		 
        if ($act == 'logo'){      				 	
        ?>
        <table width="100%" style='padding:0;margin:0;' cellspacing='0' cellpadding='0' class="tableBorder">
            <th width="30%"><p align="center">图片名称</p></th>
            <th width="20%"><p align="center">当前图片</p></th>
            <th width="50%"><p align="center">上传文件</p></th>  
            <form enctype="multipart/form-data" method="post" action="save.php?type=base">
                <tr>
                    <td><label><p align="center">上传LOGO(大小为220*60)</p></label></td>
                    <td><p align="center">  <img src="include/logo.png" style="width:auto;height:40px;"></p></td>
                    <td><p align="center"><input name="logo.png" type="file" /> <input name="" type="Submit" class="button" value="保存" /></p></td>
                </tr>        
            </form>
            <form enctype="multipart/form-data" method="post" action="save.php?type=pic">
                <tr>
                    <td><label><p align="center">上传默认缩略图(大小为300*300)</p></label></td>
                    <td><p align="center"><img src="include/pic.png" style="width:auto;height:40px;"></p></td>
                    <td><p align="center"><input name="pic.png" type="file" /><input name="" type="Submit" class="button" value="保存" /></p></td>
                </tr>            
            </form>  
        </table>  
        <?php
        }    	 	  		 
        if ($act == 'seo'){      				 	
        ?>
        <form id="form1" name="form1" method="post">  
            <table width="100%" style='padding:0;margin:0;' cellspacing='0' cellpadding='0' class="tableBorder">
                <tr>
                    <th width="15%"><p align="center">配置名称</p></th>
                    <th width="50%"><p align="center">配置内容</p></th>
                    <th width="25%"><p align="center">配置说明</p></th>
                </tr>
                <tr>
                    <td><label><p align="center">网站首页标题</p></label></td>
                    <td><p align="left"><textarea name="titles" type="text" id="titles" style="width:98%;"><?php echo $zbp->Config('tx_freecms')->titles;?></textarea></p></td>
                    <td><p align="left">默认调用网站设置里面的标题+副标题，此处填写后会自动调用你填写的内容</p></td>
                </tr>
                <tr>
                    <td><label><p align="center">网站描述</p></label></td>
                    <td><p align="left"><textarea name="description" type="text" id="description" style="width:98%;"><?php echo $zbp->Config('tx_freecms')->description;?></textarea></p></td>
                    <td><p align="left">首页网站描述</p></td>
                </tr>
                <tr>
                    <td><label><p align="center">网站关键词</p></label></td>
                    <td><p align="left"><textarea name="keywords" type="text" id="keywords" style="width:98%;"><?php echo $zbp->Config('tx_freecms')->keywords;?></textarea></p></td>
                    <td><p align="left">首页网站关键词</p></td>
                </tr>
                <tr>
                    <td><label><p align="center">SEO开关</p></label></td>
                    <td><p align="center"><input name="seoon" type="text" value="<?php echo $zbp->Config('tx_freecms')->seoon; ?>" class="checkbox" style="display:none;" /></p></td>
                    <td><p align="left">本主题支持首页、列表页、内容页的标题/关键词/描述手工填写选项，如果你安装了“seo”类插件后产生冲突，请关闭此开关即可！</p></td>
                </tr>
                <tr>
                    <td><label><p align="center">标题连接符</p></label></td>
                    <td><p align="left"><textarea name="connector" type="text" id="connector" style="width:98%;"><?php echo $zbp->Config('tx_freecms')->connector;?></textarea></p></td>
                    <td><p align="left">标题之间的连接符，可以是-可以是_也可以是/，可以是任何你觉得合适的符号</p></td>
                </tr>
                <tr>
                    <td><label><p align="center">内容描述摘要文章内容还是摘要？</p></label></td>
                    <td><p align="center"><input name="introon" type="text" value="<?php echo $zbp->Config('tx_freecms')->introon; ?>" class="checkbox" style="display:none;" /></p></td>
                    <td><p align="left">默认调用文章内容前100个字符，打开此开关则调用文章摘要（<span style="color:#f00">打开开关后请务必手动设置每篇文章的摘要保证没有错误代码，否则会导致页面错乱等问题</span>）</p></td>
                </tr>
				<tr>
                    <td><label><p align="center">卸载主题是否保留配置？</p></label></td>
                    <td><p align="center"><input name="configureon" type="text" value="<?php echo $zbp->Config('tx_freecms')->configureon; ?>" class="checkbox" style="display:none;" /></p></td>
                    <td><p align="left" style="color:#f00">打开此开关卸载主题的时候主题配置将被一并清除，请慎重操作！！！</p></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <input name="" type="Submit" class="button" value="保存"/>
                    </td>
                </tr>
            </table> 
        </form>
        <?php
        }    	 	  		 
        if ($act == 'home'){      				 	
        ?>
        <form id="form1" name="form1" method="post">  
            <table width="100%" style='padding:0;margin:0;' cellspacing='0' cellpadding='0' class="tableBorder">
                <tr>
                    <th width="15%"><p align="center">配置名称</p></th>
                    <th width="50%"><p align="center">配置内容</p></th>
                    <th width="25%"><p align="center">配置说明</p></th>
                </tr>

                <tr>
                    <td><label><p align="center">顶部左侧自定义内容，留空则调用网站副标题</p></label></td>
                    <td><p align="left"><textarea name="topl" type="text" id="topl" style="width:98%;"><?php echo $zbp->Config('tx_freecms')->topl;?></textarea></p></td>
                    <td><p align="left">支持html代码</p></td>
                </tr>
                <tr>
                    <td><label><p align="center">首页flash幻灯片</p></label></td>
                    <td><p align="left"><textarea name="flash" type="text" id="flash" style="width:98%;" rows="7"><?php echo $zbp->Config('tx_freecms')->flash;?></textarea></p></td>
                    <td><p align="left">进修改文字链接图片地址即可，代码格式勿动。关掉 <input name="flashon" type="text" value="<?php echo $zbp->Config('tx_freecms')->flashon; ?>" class="checkbox" style="display:none;" /> 则调用热门文章</p></td>
                </tr>
                <tr>
                    <td><label><p align="center">首页cms模块调用分类</p></label></td>
                    <td><p align="left"><textarea name="cms" type="text" id="cms" style="width:98%;"><?php echo $zbp->Config('tx_freecms')->cms;?></textarea></p></td>
                    <td><p align="left">仅填写分类id即可，多个id之间用英文逗号,隔开，每个分类模块的显示样式请前往对应分类编辑里面设置</p></td>
                </tr>
                <tr>
                    <td><label><p align="center">logo右侧广告位</p></label></td>
                    <td><p align="left"><textarea name="gg1" type="text" id="gg1" style="width:98%;"><?php echo $zbp->Config('tx_freecms')->gg1;?></textarea></p></td>
                    <td><p align="left">宽460高60</p></td>
                </tr>
                <tr>
                    <td><label><p align="center">导航栏下通栏广告位</p></label></td>
                    <td><p align="left"><textarea name="gg2" type="text" id="gg2" style="width:98%;"><?php echo $zbp->Config('tx_freecms')->gg2;?></textarea></p></td>
                    <td><p align="left">宽1200高随便</p></td>
                </tr>
                <tr>
                    <td><label><p align="center">底部通栏广告位</p></label></td>
                    <td><p align="left"><textarea name="gg3" type="text" id="gg3" style="width:98%;"><?php echo $zbp->Config('tx_freecms')->gg3;?></textarea></p></td>
                    <td><p align="left">宽1200高随便</p></td>
                </tr>
                <tr>
                    <td colspan="3">
                        <input name="" type="Submit" class="button" value="保存"/>
                    </td>
                </tr>
            </table> 
        </form>
        <?php
        }    	 	  		    				 	
        ?>
    </div>
</div>
<script type="text/javascript">ActiveTopMenu("topmenu_tx_freecms");</script> 
<?php
require $blogpath . 'zb_system/admin/admin_footer.php';
RunTime();
?>
