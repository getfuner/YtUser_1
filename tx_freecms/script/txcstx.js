$(function(){
    var winr=$(window); 
    var surl = location.href;
    var surl2 = $(".place a:eq(1)").attr("href");
    
    $(".nav li a").each(function() {
        if ($(this).attr("href")==surl || $(this).attr("href")==surl2) $(this).parent().addClass("on");
    });
    
    $(".nav-on").click(function () {
        $(".nav").slideToggle();
    });
    $(".search-on").click(function () {
        $(".search").slideToggle();
    });
});

function tabs(tabTit,on,tabCon){
    $(tabTit).children().hover(function(){
        $(this).addClass(on).siblings().removeClass(on);
        var index = $(tabTit).children().index(this);
        $(tabCon).children().eq(index).show().siblings().hide();
    });
};
tabs(".tab-hd","on",".tab-bd");



zbp.plugin.unbind("comment.reply", "system");
zbp.plugin.on("comment.reply", "tx_freecms", function(id) {
    var i = id;
    $("#inpRevID").val(i);
    var frm = $('#divCommentPost'),
        cancel = $("#cancel-reply");

    frm.before($("<div id='temp-frm' style='display:none'>")).addClass("reply-frm");
    $('#AjaxComment' + i).before(frm);

    cancel.show().click(function() {
        var temp = $('#temp-frm');
        $("#inpRevID").val(0);
        if (!temp.length || !frm.length) return;
        temp.before(frm);
        temp.remove();
        $(this).hide();
        frm.removeClass("reply-frm");
        return false;
    });
    try {
        $('#txaArticle').focus();
    } catch (e) {}
    return false;
});

zbp.plugin.on("comment.get", "tx_freecms", function (logid, page) {
    $('span.commentspage').html("提交中...");
});
zbp.plugin.on("comment.got", "tx_freecms", function (logid, page) {
    $("#cancel-reply").click();
});
zbp.plugin.on("comment.postsuccess", "tx_freecms", function () {
    $("#cancel-reply").click();
});