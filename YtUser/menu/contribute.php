<?php	  		 	
require '../../../../zb_system/function/c_system_base.php';    				 	 	     	   	 	    							 				
require '../../../../zb_system/function/c_system_admin.php';    	 		   				     			    	    	 	
$zbp->Load();	 	   	 				 	 	
$action='root';     	     	   	   		 	    	 			 	 
if (!$zbp->CheckRights($action)) {$zbp->ShowError(6);die();}    	    			    	 	    	
if (!$zbp->CheckPlugin('YtUser')) {$zbp->ShowError(48);die();}     					 	  	  	  

if (isset($_GET['act'])){$act = $_GET['act'];}else{$act = 'base';}    	    	  								

$blogtitle='用户中心';     	  		 	 	 	    			
require $blogpath . 'zb_system/admin/admin_header.php';    						 	   			   	 	
require $blogpath . 'zb_system/admin/admin_top.php';   	     	  					    	    		
?>
<div id="divMain">
    <div class="divHeader"><?php echo $blogtitle;?></div>
    <div class="SubMenu">
        <?php echo YtUser_SubMenu('contribute'); ?>
        <a href="http://www.kancloud.cn/showhand/zbloguser" target="_blank"><span class="m-left" style="color:#F00">适配教程</span></a>
        <?php if ($act == 'buy'){?>
        <a href="?act=buy&buystate=nopay"><span class="m-right" style="color:red">未付款</span></a>
        <a href="?act=buy&buystate=paid"><span class="m-right" style="color:green">已付款</span></a>
        <?php }?>
    </div>
    <div id="divMain2">
        <form enctype="multipart/form-data" method="post" action="save.php?type=contribute">  
            <input id="reset" name="reset" type="hidden" value="">
            <table border="1" class="tableFull tableBorder">
                <tr>
                    <th><p align='left'><b>选项</b><br><span class='note'></span></p></th>
                    <th>说明</th>
                </tr>
                
                <tr>
                    <td><p align='left'><b>是否开启投稿送<?php echo $zbp->Config('YtUser')->jifenname ;?></b></p></td>
                    <td><input type="text" class="checkbox" name="contribute_on" value="<?php echo $zbp->Config('YtUser')->contribute_on;?>"></td>
                </tr>
                <tr>
                    <td><p align='left'><b>投稿成功奖励<?php echo $zbp->Config('YtUser')->jifenname ;?></b></p></td>
                    <td><input type="text" name="contribute_jf" style="width:150px;" value="<?php echo (int)$zbp->Config('YtUser')->contribute_jf ? $zbp->Config('YtUser')->contribute_jf : "5" ;?>" style="width:89%;"></td>
                </tr>
                <tr>
                    <td><p align='left'><b>可以投稿的用户最低等级</b></p></td>
                    <td>
                        <select class="yt-level edit" name="contribute_level" id="contribute_level">
                            <option value="1" ><?php echo $zbp->lang['user_level_name'][1];?></option><option value="2"><?php echo $zbp->lang['user_level_name'][2];?></option><option value="3"><?php echo $zbp->lang['user_level_name'][3];?></option><option value="4" selected="selected"><?php echo $zbp->lang['user_level_name'][4];?></option><option value="5"  ><?php echo $zbp->lang['user_level_name'][5];?></option>
                        </select>
                        <p>设置完后还需要通过“<a href="<?php echo $zbp->host; ?>zb_users/plugin/AppCentre/main.php?id=235" target="_blank">分类权限角色分配器</a>”插件来设置相关用户组仅能在哪个分类投稿。</p>
                    </td>
                </tr>
                <tr>
                    <td><p align='left'><b>投稿后的文章状态：</b></p></td>
                    <td>
                        <select class="yt-status edit" name="contribute_status" id="contribute_status">
                            <option value="0">公开</option>
                            <option value="1">草稿</option>
                            <option value="2" selected="selected">审核</option> 
                        </select>
                    </td>
                </tr>
            </table>
            <hr/>
            <p>
                <input type="submit" class="button" value="<?php echo $lang['msg']['submit']?>">
            </p>
        </form>

        <script type="text/javascript">
            ActiveLeftMenu("aPluginMng");
            var x = <?php echo $zbp->Config('YtUser')->contribute_status;?>;
            var y = <?php echo $zbp->Config('YtUser')->contribute_level;?>;
            $(".yt-status").val(x);
            $(".yt-level").val(y);
        </script>

        <script type="text/javascript">AddHeaderIcon("<?php echo $bloghost . 'zb_users/plugin/YtUser/logo.png';?>");</script>	

    </div>

</div>



<?php

require $blogpath . 'zb_system/admin/admin_footer.php';          

RunTime();     	

?>