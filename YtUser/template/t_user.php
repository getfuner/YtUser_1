{* Template Name: 个人资料 *}
<?php echo'<meta charset="UTF-8"><div style="text-align:center;padding:60px 0;font-size:16px;"><h2 style="font-size:60px;margin-bottom:32px;">打开这个网页的是傻逼</h2>吼吼!</div>';die();?>

{template:t_header}
<div class="col-19 col-m-24">
    <div class="user-information">
        <ul class="row">
            <li class="col-6 col-m-12 mb15"><a href="{$host}{$zbp->Config('YtUser')->YtUser_Upgrade}" class="bg-cyan"><i class="iconfont icon-crown"></i>   {if $user.Level <= 4}VIP会员{else}普通用户{/if}</a></li>
            <li class="col-6 col-m-12 mb15"><a href="{$host}{$zbp->Config('YtUser')->YtUser_Integral}" class="bg-blue"><i class="iconfont icon-recharge"></i> 余额 {$user.YtUser('Price')} </a></li>
            <li class="col-6 col-m-12 mb15"><a href="{$host}{$zbp->Config('YtUser')->YtUser_Articlelist}" class="bg-orange"><i class="iconfont icon-copy"></i> 文章 {$zbp->user->Articles}</a></li>
            <li class="col-6 col-m-12 mb15"><a href="{$host}{$zbp->Config('YtUser')->YtUser_Commentlist}" class="bg-green"><i class="iconfont icon-liuyan"></i> 评论 {$zbp->user->Comments}</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-12 col-m-24 mb15">
            <div class="tx-box bg-white">
                <h2 class="tx-title">最新文章</h2>
                <ul class="ul-32 pd15-4">
                    {foreach GetList(9) as $newlist}
                    <li><span class="fr f-gray1 ml10">{$newlist.Time('m-d')}</span><a href="{$newlist.Url}" title="{$newlist.Title}">{$newlist.Title}</a></li>
                    {/foreach}
                </ul>
            </div>
        </div>
        <div class="col-12 col-m-24 mb15">
            <div class="tx-box bg-white">
                <h2 class="tx-title">个人信息</h2>
                <div class="tx-form-li pd15">
                    <ul class="row">
                        <form role="form" action="#" method="POST" id="signup-form">
                            <input id="edtID" name="ID" type="hidden" value="{$user.ID}">
                            <input id="edtGuid" name="Guid" type="hidden" value="{$user.Guid}">
                            <li class="col-12 col-m-24"><p><input required="required" type="text" id="edtAlias" name="Alias" value="{$user.StaticName}" class="tx-input"><i>昵称(*)</i></p></li>
                            <li class="col-12 col-m-24"><p><input type="text" id="edtEmail" name="Email" value="{$user.Email}" class="tx-input"><i>邮箱(*)</i></p></li>
                            <li class="col-12 col-m-24"><p><input type="text" id="edtHomePage" name="HomePage" value="{$user.HomePage}"  class="tx-input"><i>网站</i></p></li>
                            <li class="col-12 col-m-24"><p><input required="required" type="text" id="meta_Tel" name="meta_Tel" value="{$user.Metas.Tel}" class="tx-input"><i>电话</i></p></li>
                            <li class="col-12 col-m-24"><p><input required="required" type="text" id="meta_Add" name="meta_Add" value="{$user.Metas.Add}" class="tx-input"><i>地址</i></p></li>
                            <li class="input-ma col-12 col-m-24"><p><input required="required" type="text" name="verifycode"  class="tx-input" placeholder="验证码">{$article.verifycode}</p></li>
                            <li class="col-24 col-m-24"><p><textarea id="edtIntro" name="Intro" class="tx-textarea" placeholder="个人简介" style="height:74px;">{$user.Intro}</textarea></p></li>
                            <li class="col-24 col-m-24"><p class="tx-input-full"><button onclick="return checkInfo();" class="tx-btn tx-btn-big bg-black">确定修改</button></p></li>
                        </form>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
{template:t_footer}
