{* Template Name: 菜单*}
<?php echo'<meta charset="UTF-8"><div style="text-align:center;padding:60px 0;font-size:16px;"><h2 style="font-size:60px;margin-bottom:32px;">这里是用户中心模版</h2>哈哈</div>';die();?>
<dl class="side-menu-box mb15">
    <dd>
        <ul>
            <li><a href="{$host}{$zbp->Config('YtUser')->YtUser_User}"><i class="iconfont icon-shouye"></i> 用户首页</a></li>

            <li><a href="{$host}{$zbp->Config('YtUser')->YtUser_Articlelist}"><i class="iconfont icon-text"></i> 文章管理</a></li>
            {if $zbp->user->Level <= $zbp->Config('YtUser')->contribute_level} 
            <li><a href="{$host}{$zbp->Config('YtUser')->YtUser_Articleedt}"><i class="iconfont icon-write"></i> 发布文章</a></li>
            {/if}
            <li><a href="{$host}{$zbp->Config('YtUser')->YtUser_Commentlist}"><i class="iconfont icon-liuyan"></i> 评论管理</a></li>
            <li><a href="{$host}{$zbp->Config('YtUser')->YtUser_Favorite}"><i class="iconfont icon-xihuan"></i> 我的收藏</a></li>
        </ul>
    </dd>
</dl>


<dl class="side-menu-box mb15">
    <dd>
        <ul>
            <li><a href="{$host}{$zbp->Config('YtUser')->YtUser_Integral}"><i class="iconfont icon-refund"></i> {$zbp->Config('YtUser')->jifenname}充值</a></li>
            <li><a href="{$host}{$zbp->Config('YtUser')->YtUser_Upgrade}"><i class="iconfont icon-vip"></i> VIP充值</a></li>
            <li><a href="{$host}{$zbp->Config('YtUser')->YtUser_Paylist}"><i class="iconfont icon-calendar"></i> 购买列表</a></li>
            <li><a href="{$host}{$zbp->Config('YtUser')->YtUser_Consume}"><i class="iconfont icon-calendar"></i> 消费记录</a></li>
        </ul>
    </dd>
</dl>


<dl class="side-menu-box mb15">
    <dd>
        <ul>
            <li><a href="{$host}{$zbp->Config('YtUser')->YtUser_Changepassword}"><i class="iconfont icon-lock"></i> 修改密码</a></li>
            {if $zbp->Config('YtUser')->appid}
            <li><a href="{$host}{$zbp->Config('YtUser')->YtUser_Binding}"><i class="iconfont icon-qq2"></i> 账号绑定</a></li>
            {/if}
            {if $zbp->Config('YtUser')->Oncertif=='1'}
            <li><a href="{$host}{$zbp->Config('YtUser')->YtUser_Certifi}"><i class="iconfont icon-pengyou"></i> 实名认证</a></li>
            {/if}
            <li><a href="{$host}zb_users/plugin/YtUser/loginout.php"><i class="iconfont icon-guanbi1"></i> 退出账号</a></li>
        </ul>
    </dd>
</dl>